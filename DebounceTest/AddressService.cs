﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace DebounceTest
{
    public class AddressService
    {
		public static GoogleStreetAddressResponse QueryForAddressList(string address)
		{
			GoogleStreetAddressResponse googleAddressResponse = new GoogleStreetAddressResponse();

			try
			{
				string APIKey = "AIzaSyDK2e5IvDmbTohlIVP-AKEY-E9yylkrYz4";
				//Your code goes here
				HttpClient client = new HttpClient();
				client.BaseAddress = new Uri(string.Format("https://maps.googleapis.com/maps/api/place/autocomplete/json?key={0}&types=address&input={1}&components=country:za", APIKey, System.Net.WebUtility.UrlEncode(address)));

				// Add an Accept header for XML format.
				client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
				client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

				// List data response.
				HttpResponseMessage response = client.GetAsync(string.Empty).Result;

				if (response.IsSuccessStatusCode)
				{
					string output = response.Content.ReadAsStringAsync().Result;

					googleAddressResponse = JsonConvert.DeserializeObject<GoogleStreetAddressResponse>(output);
					return googleAddressResponse;
				}
			}
			catch (Exception e)
			{
				throw e;
			}

			return googleAddressResponse;
		}

        public static GoogleDetailStreetAddressResponse QueryForAddressDetail(string placeId)
		{
			GoogleDetailStreetAddressResponse googleAddressResponse = new GoogleDetailStreetAddressResponse();

			try
			{
				string APIKey = "AIzaSyDK2e5IvDmbTohlIVP-AKEY-E9yylkrYz4";
				//Your code goes here
				HttpClient client = new HttpClient();
				client.BaseAddress = new Uri(string.Format("https://maps.googleapis.com/maps/api/place/details/json?placeid={0}&key={1}", placeId, APIKey));

				// Add an Accept header for XML format.
				client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
				client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

				// List data response.
				HttpResponseMessage response = client.GetAsync(string.Empty).Result;

				if (response.IsSuccessStatusCode)
				{
					string output = response.Content.ReadAsStringAsync().Result;

					googleAddressResponse = JsonConvert.DeserializeObject<GoogleDetailStreetAddressResponse>(output);
					return googleAddressResponse;
				}
			}
			catch (Exception e)
			{
				throw e;
			}

			return googleAddressResponse;
		}
    }
}
