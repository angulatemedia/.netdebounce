﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace DebounceTest
{
    class Program
    {
        static void Main(string[] args)
        {

            Other o = new Other();
            string text = string.Empty;
            Subject<string> typingSubject = new Subject<string>();
            IDisposable typingEventSequence;

            typingEventSequence = typingSubject.Throttle(TimeSpan.FromSeconds(1))
                                               .Subscribe(async query =>
                                               {
                                                   await o.QueryGoogleStreetList(query);
                                                   Console.WriteLine("result count:");
                                                   foreach (var item in o.AddressList)
                                                   {
                                                       Console.WriteLine(item.CombineAddress);
                                                   }
                                               });

            bool readflag = true;
            while (readflag == true)
            {
                try
                {
                    ConsoleKeyInfo info = Console.ReadKey();
                    if (info.Key == ConsoleKey.Escape)
                    {
                        Console.WriteLine("You exited");
                        return;
                    }

                    if (info.Key == ConsoleKey.Backspace)
                    {
                        text = text.Remove(text.Length - 1);
                    }

                    text += info.KeyChar;

                    Console.WriteLine("");
                    Console.WriteLine(text);

                    typingSubject.OnNext(text.Trim());
                }
                catch (Exception)
                {
                }
            }
        }
    }
}