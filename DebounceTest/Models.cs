﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;

namespace DebounceTest
{
    public class GoogleAddressModel
    {
        public string PlaceID { get; set; }
        public string CombineAddress { get; set; }

        public GoogleAddressModel(string PlaceID, string CombineAddress)
        {
            this.PlaceID = PlaceID;
            this.CombineAddress = CombineAddress;
        }
    }

    public class ResultBase
    {
        /// <summary>
        /// Success of service call. False is failed.
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Success or Cause of failure reason
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Type of the failure retrieved from the service call.  
        /// </summary>
        public FailureType TypeOfFailure { get; set; }

        /// <summary>
        /// Status code retrieved from the service call.  
        /// </summary>
        public HttpStatusCode StatusCode { get; set; }

        public enum FailureType
        {
            ClientException,
            ServerException,
            ServiceClientException,
            Exception,
            Offline,
            OAuthAuthorizationException
        }
    }

    public class GoogleStreetListResult : ResultBase
    {
        public ObservableCollection<GoogleAddressModel> AddressList { get; set; }
    }

    public class GoogleDetailStreetAddressResponse
    {
        public List<object> html_attributions { get; set; }
        public Result result { get; set; }
        public string status { get; set; }
        public string error_message { get; set; }

        public void Dispose()
        {
            Dispose();
            GC.SuppressFinalize(this);
        }
    }

    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }

    public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Geometry
    {
        public Location location { get; set; }
        public Viewport viewport { get; set; }
    }

    public class Result
    {
        public List<AddressComponent> address_components { get; set; }
        public string adr_address { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string icon { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string place_id { get; set; }
        public string reference { get; set; }
        public string scope { get; set; }
        public List<string> types { get; set; }
        public string url { get; set; }
        public int utc_offset { get; set; }
        public string vicinity { get; set; }
    }

	public class GoogleStreetAddressResponse
	{
		public List<Prediction> predictions { get; set; }
		public string status { get; set; }
		public string error_message { get; set; }
	}

	public class MatchedSubstring
	{
		public int length { get; set; }
		public int offset { get; set; }
	}

	public class MainTextMatchedSubstring
	{
		public int length { get; set; }
		public int offset { get; set; }
	}

	public class StructuredFormatting
	{
		public string main_text { get; set; }
		public List<MainTextMatchedSubstring> main_text_matched_substrings { get; set; }
		public string secondary_text { get; set; }
	}

	public class Term
	{
		public int offset { get; set; }
		public string value { get; set; }
	}

	public class Prediction
	{
		public string description { get; set; }
		public string id { get; set; }
		public List<MatchedSubstring> matched_substrings { get; set; }
		public string place_id { get; set; }
		public string reference { get; set; }
		public StructuredFormatting structured_formatting { get; set; }
		public List<Term> terms { get; set; }
		public List<string> types { get; set; }
	}
}
