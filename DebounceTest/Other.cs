﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;

namespace DebounceTest
{
    public class Other
    {
        public bool IsBusy { get; set; }
        private int totalAsyncCount = 0;
        public int totalEventsFired = 0;
        public ObservableCollection<GoogleAddressModel> AddressList { get; set; }

        public GoogleStreetListResult QueryGoogleAddress(string address)
        {
            GoogleStreetAddressResponse response = AddressService.QueryForAddressList(address);
            return GoogleStreetAddressListToSuccessResult(response);

        }

        public GoogleStreetListResult GoogleStreetAddressListToSuccessResult(GoogleStreetAddressResponse response)
        {
            GoogleStreetListResult googleStreetListResult = new GoogleStreetListResult()
            {
                Success = true,
                AddressList = new ObservableCollection<GoogleAddressModel>()
            };

            foreach (Prediction predicate in response.predictions)
            {
                googleStreetListResult.AddressList.Add(new GoogleAddressModel(predicate.place_id, predicate.description));
            }

            AddressList = googleStreetListResult.AddressList;

            return googleStreetListResult;
        }

        public async Task<GoogleStreetListResult> QueryGoogleStreetList(string searchTerm)
        {
            totalEventsFired++;
            Console.WriteLine(string.Format("Events fired = {0}",totalEventsFired));
            return await Task.Run(() => QueryGoogleAddress(searchTerm));
        }

        public Task DoAsync(Action action)
        {
            return Task.Run(() =>
             {
                 try
                 {
                     IsBusy = true;
                     Interlocked.Increment(ref totalAsyncCount); //interlocked increment

                     action();
                 }
                 catch (Exception ex)
                 {
                     Console.WriteLine(ex.Message);
                     Console.WriteLine(ex.StackTrace);
                 }

             })
             .ContinueWith(continueAction =>
             {
                 Interlocked.Decrement(ref totalAsyncCount);

                 if (totalAsyncCount == 0 && totalAsyncCount == 0) //prevent multiple Async overrides 
                 {
                     IsBusy = false;
                 }
             });
        }

        private void HandleAddressList(GoogleStreetListResult response)
        {
            if (response.Success)
            {
                AddressList = response.AddressList;
            }
        }
    }
}
